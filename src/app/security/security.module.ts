import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthConfig, AuthService } from './auth.service';
import { environment } from '../../environments/environment';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers:[
    {
      provide:AuthConfig,
      useValue: environment.authConfig
    }
  ]
})
export class SecurityModule {

  constructor(private auth:AuthService){
    // Check if token is available:
    this.auth.getToken()
  }
 }
