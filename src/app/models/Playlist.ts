import { Track } from './Track';

export interface Playlist {
    id: number;
    name: string;
    favourite: boolean;
    /**
     * HEX color 
     */
    color: string;
    tracks?: Track[]
}

