export interface Entity {
  id: string;
  name: string;
}

/* === */

export interface Album extends Entity {
  images: AlbumImage[];
  artists?: Artist[];
}

export interface AlbumImage {
  url: string;
}

export interface Artist extends Entity {}

/* === PAGINATION === */

export interface PagingObject<T> {
  items: T[];
  limit?: number;
  offset?: number;
  total?: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
