import { Component, OnInit } from '@angular/core';
import { TestingService } from '../testing.service';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.scss']
})
export class TestingComponent implements OnInit {

  message = 'testing works!'

  constructor(private service:TestingService) { }

  ngOnInit() {
  }

  save(){
    this.service.save(this.message)
  }

}
