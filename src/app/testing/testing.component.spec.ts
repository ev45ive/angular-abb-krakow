import { async, ComponentFixture, TestBed, inject } from "@angular/core/testing";

import { TestingComponent } from "./testing.component";
import { By } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { TestingService } from "../testing.service";

fdescribe("TestingComponent", () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestingComponent],
      imports: [FormsModule],
      providers: [
        {
          provide: TestingService,
          useValue: jasmine.createSpyObj(["save"])
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should render message", () => {
    expect(fixture.nativeElement.innerHTML).toMatch("testing works!");
  });

  it("should render changed message", () => {
    component.message = "Placki!";
    fixture.detectChanges();
    const elem = fixture.debugElement.query(By.css("p.message"));

    expect(elem.nativeElement.innerHTML).toMatch("Placki!");
  });

  it("should render input with message", () => {
    const elem = fixture.debugElement.query(By.css("input"));

    return fixture.whenStable().then(() => {
      expect(elem.nativeElement.value).toEqual(component.message);
    });
  });

  it("should update message when input value changes", () => {
    const elem = fixture.debugElement.query(By.css("input"));
    elem.nativeElement.value = "Placki";

    // elem.nativeElement.dispatchEvent(new Event('input'))
    elem.triggerEventHandler("input", {
      target: elem.nativeElement
    });

    expect(component.message).toEqual("Placki");
  });

  it("should save when button is clicked", () => {
    const elem = fixture.debugElement.query(By.css("[type=button],button"));
    const spy = spyOn(component, "save");

    elem.nativeElement.dispatchEvent(new Event("click"));

    expect(spy).toHaveBeenCalled();
  });

  it("shoud call save on message service", 
  inject([TestingService],(service:jasmine.SpyObj<TestingService>) => {

    component.save()
    expect(service.save).toHaveBeenCalledWith(component.message)
  }));
});
