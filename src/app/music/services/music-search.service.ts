import {
  Injectable,
  Inject,
  InjectionToken,
  EventEmitter
} from "@angular/core";
import { Album, AlbumsResponse } from "../../models/Album";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../../security/auth.service";

import {
  map,
  catchError,
  concat,
  startWith,
  switchAll,
  switchMap
} from "rxjs/operators";
import {
  empty,
  throwError,
  of,
  Subject,
  ReplaySubject,
  BehaviorSubject,
  Observable
} from "rxjs";

export const SEARCH_URL = new InjectionToken("Search API Url");

@Injectable({
  providedIn: "root" // MusicModule
})
export class MusicSearchService {
  private albumsChange = new BehaviorSubject<Album[]>([]);
  private queryChange = new BehaviorSubject<string>("batman");

  constructor(
    @Inject(SEARCH_URL)
    private search_url: string,
    private http: HttpClient,
    private auth: AuthService
  ) {
    this.queryChange
      .pipe(
        map(query => ({
          type: "album",
          q: query
        })),
        switchMap(params =>
          this.http
            .get<AlbumsResponse>(this.search_url, {
              headers: {
                Authorization: `Bearer ${this.auth.getToken()}`
              },
              params
            })
            .pipe(
              catchError((error, caught) => {
                if (
                  error instanceof HttpErrorResponse &&
                  error.status === 401
                ) {
                  this.auth.authorize();
                }
                return throwError(new Error(error.error.error.message));
              })
            )
        ),
        // switchAll(),
        map(resp => resp.albums.items)
      )
      // .subscribe(this.albumsChange)
      .subscribe(albums => {
        this.albumsChange.next(albums);
      });
  }

  /* Comands - Inputs: */
  search(query: string) {
    this.queryChange.next(query);
  }

  /* Queries - Outputs: */

  getQuery() {
    return this.queryChange.asObservable();
  }

  getAlbums() {
    return this.albumsChange.asObservable();
  }

}
