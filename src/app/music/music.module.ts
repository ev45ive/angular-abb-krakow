import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicRoutingModule } from "./music-routing.module";
import { MusicSearchViewComponent } from "./music-search-view/music-search-view.component";
import { SearchFormComponent } from "./search-form/search-form.component";
import { AlbumsGridComponent } from "./albums-grid/albums-grid.component";
import { AlbumCardComponent } from "./album-card/album-card.component";
import { environment } from "../../environments/environment";
import { SEARCH_URL } from "./services/music-search.service";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule, FormBuilder, FormControl } from "@angular/forms";
import { MusicProviderDirective } from './music-provider.directive';

@NgModule({
  declarations: [
    MusicSearchViewComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumCardComponent,
    MusicProviderDirective
  ],
  imports: [
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    MusicRoutingModule
  ],
  exports: [MusicSearchViewComponent, MusicProviderDirective],
  providers: [
    {
      provide: SEARCH_URL,
      useValue: environment.api_url
    }
    // {
    //   provide: MusicSearchService,
    //   useFactory: (url:string) => {
    //     return new MusicSearchService(url)
    //   },
    //   deps:[SEARCH_URL]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: SpecialMusicSearchService,
    //   // deps: [SEARCH_URL]
    // },
    // MusicSearchService,
  ]
})
export class MusicModule {}
