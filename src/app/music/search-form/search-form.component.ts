import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormArray,
  AbstractControl,
  FormBuilder,
  Validators,
  ValidatorFn,
  Validator,
  AsyncValidatorFn,
  ValidationErrors
} from "@angular/forms";
import {
  distinctUntilChanged,
  filter,
  debounceTime,
  combineLatest,
  withLatestFrom
} from "rxjs/operators";
import { Observable, Observer } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  queryForm: FormGroup;

  @Input()
  set query(q: string) {
    this.queryForm.get("query")!.setValue(q,{
      emitEvent:false,
      // onlySelf:true
    });
  }

  constructor() {
    const censor: ValidatorFn = (
      control: AbstractControl
    ): ValidationErrors | null => {
      const hasError = (control.value as string).includes("batman");

      return hasError
        ? {
            censor: { badword: "batman" }
          }
        : null;
    };

    const asyncCensor: AsyncValidatorFn = (
      control: AbstractControl
    ): Observable<ValidationErrors | null> => {
      // return this.http.post(/validate, value).pipe(map(res=>err))

      return Observable.create(
        (observer: Observer<ValidationErrors | null>) => {
          const handler = setTimeout(() => {
            const hasError = (control.value as string).includes("batman");

            observer.next(
              hasError
                ? {
                    censor: { badword: "batman" }
                  }
                : null
            );
            observer.complete();
          }, 2000);

          // onUnsubscribe:
          return () => {
            clearTimeout(handler);
          };
        }
      );
    };

    this.queryForm = new FormGroup({
      query: new FormControl(
        "",
        [Validators.required, Validators.minLength(3) /* , censor */],
        [asyncCensor]
      )
    });

    const value$ = this.queryForm.get("query")!.valueChanges.pipe(
      debounceTime(400),
      filter((query: string) => query.length >= 3),
      distinctUntilChanged()
    );
    const status$ = this.queryForm.get("query")!.statusChanges;

    const valid$ = status$.pipe(filter(status => status === "VALID"));

    const search$ = valid$.pipe(
      // combineLatest(value$, (valid,value)=>value),
      withLatestFrom(value$, (valid, value) => value)
    );

    search$.subscribe(query => {
      this.search(query);
    });

    console.log(this.queryForm);
  }

  @Output()
  queryChange = new EventEmitter<string>();

  search(query: string) {
    // console.log(query)
    this.queryChange.emit(query);
  }

  ngOnInit() {}
}
