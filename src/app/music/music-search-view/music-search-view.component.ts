import { Component, OnInit, Inject } from "@angular/core";
import { Album } from "../../models/Album";
import { MusicSearchService } from "../services/music-search.service";
import { Subscription, Subject, empty } from "rxjs";
import { takeUntil, tap, catchError } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search-view",
  templateUrl: "./music-search-view.component.html",
  styleUrls: ["./music-search-view.component.scss"]
  // viewProviders:[
  //   MusicSearchService
  // ]
})
export class MusicSearchViewComponent implements OnInit {
  albums$ = this.service.getAlbums();
  query$ = this.service.getQuery();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) {}

  search(query: string) {
    this.service.search(query);

    if (query) {
      this.router.navigate(["/music"], {
        queryParams: {
          'query':query
        },
        replaceUrl:true
      });
    }
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      const query = params.get("query");

      if (query) {
        this.search(query);
      }
    });
  }
}
