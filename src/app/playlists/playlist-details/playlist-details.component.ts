import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Playlist } from 'src/app/models/Playlist';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist: Playlist

  @ViewChild('formRef', { read: NgForm })
  formRef: NgForm

  constructor() { }

  ngOnInit() {
  }

  mode = 'show'

  edit() {
    this.mode = 'edit'
  }

  cancel() {
    this.mode = 'show'
  }

  @Output()
  playlistChange = new EventEmitter<Playlist>()

  save(formRef: NgForm) {
    const draft: Pick<Playlist, 'name' | 'favourite' | 'color'> = formRef.value

    const playlist = {
      ...this.playlist,
      ...draft
    }

    this.playlistChange.emit(playlist)
  }

}

// Mapped Types:
// ---
// type Partial<T> = {
//   [k in keyof T]?: T[k]
// }
