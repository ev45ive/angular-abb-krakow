import { Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/models/Playlist';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  playlists: Playlist[] = [
    {
      id: 123,
      name: 'Angular Hits',
      favourite: false,
      color: '#ff00ff'
    }, {
      id: 234,
      name: 'Angular TOP20',
      favourite: true,
      color: '#ffff00'
    }, {
      id: 345,
      name: 'Best of Angular',
      favourite: false,
      color: '#00ffff'
    },
  ]
  
  selected:Playlist = this.playlists[2];

  save(draft:Playlist){
    const index = this.playlists.findIndex(
      p => p.id === draft.id
    )
    if(index !== -1){
      this.playlists.splice(index,1,draft)
    }
    this.selected = draft
  }


  constructor() { }

  ngOnInit() {
  }

}
