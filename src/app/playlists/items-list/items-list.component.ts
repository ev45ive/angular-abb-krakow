import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from 'src/app/models/Playlist';
import { NgForOfContext } from '@angular/common';

NgForOfContext

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  // inputs:[
  //   'playlists:items',
  // ]
})
export class ItemsListComponent implements OnInit {

  hover: Playlist

  @Input('items')
  playlists: Playlist[] = []

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  @Input()
  selected: Playlist;

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist)
  }

  constructor() {

  }

  ngOnInit() {
  }

  trackFn(index: number, item: Playlist) {
    return item.id
  }
}
