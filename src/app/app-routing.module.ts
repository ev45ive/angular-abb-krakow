import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsViewComponent } from "./playlists/playlists-view/playlists-view.component";
import { MusicSearchViewComponent } from "./music/music-search-view/music-search-view.component";

const routes: Routes = [
  {
    path: "",
    // component: HomePageComponent
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path: "playlists",
    component: PlaylistsViewComponent
  },
  {
    path: "music",
    component: MusicSearchViewComponent
  },
  {
    path: "**",
    // component: PageNotFoundComponent
    redirectTo: "playlists",
    pathMatch: "full"
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true,
      // useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
