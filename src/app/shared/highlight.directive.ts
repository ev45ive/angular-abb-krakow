import { Directive, ElementRef, Input, OnInit, OnChanges, DoCheck, OnDestroy, SimpleChanges, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlight]',
  // host:{
  //   '[style.color]':'color',
  //   '(mouseenter)':'activate($event.target)'
  // }
})
export class HighlightDirective /* implements OnInit, OnChanges, DoCheck, OnDestroy  */ {

  @HostBinding('style.color')
  get currentColor(): string{
    return this.active? this.color : ''
  }

  @Input('appHighlight')
  color: string

  constructor(private elem: ElementRef) {
    // console.log("constructor");
  }

  active = false

  @HostListener('mouseenter', ['$event.screenX'])
  activate(x: number) {
    this.active = true
    // this.currentColor = this.color
  }
  
  @HostListener('mouseleave')
  deactivate() {
    this.active = false
    // this.currentColor = ''
  }





  /*   ngOnChanges(changes: SimpleChanges) {
      console.log("ngOnChanges", changes);
      // this.elem.nativeElement.style.color = this.color
    }
  
    ngOnInit() {
      console.log('ngOnInit!', this.color)
    }
  
    ngDoCheck() {
      console.log("ngDoCheck");
    }
  
    ngOnDestroy() {
      console.log("ngOnDestroy");
    }
   */
}

// console.log(HighlightDirective)