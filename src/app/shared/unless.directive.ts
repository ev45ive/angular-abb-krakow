import {
  Directive,
  TemplateRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  Input,
  ViewRef
} from "@angular/core";

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {
  viewCache: ViewRef | null;

  @Input("appUnless")
  set appUnless(hide: boolean) {
    if (hide) {
      // this.vcr.clear()
      //
      this.viewCache = this.vcr.detach();

    } else {
      if(this.viewCache){
        this.vcr.insert(this.viewCache)
      }else{
        this.vcr.createEmbeddedView(this.tpl, {
          message: "Placki",
          $implicit: "Malinowe"
        });

      }
    }
  }

  constructor(private tpl: TemplateRef<any>, private vcr: ViewContainerRef) {}
}
